## Brand book

Please find our [brand book here](Fairbee-Brandbook.pdf)

## Development methods

### Branching
Branching is an important part of our work flow to remain efficient and keep production safe from as many errors as possible.

When you begin your quest to fulfill a ticket, first thing you'll need to decide... is it a bugfix or a feature?
If you have decided, we will continue.

First fetch all branches from remote (mostly important when you start developing for the first time):
```
git fetch origin
```

After that, we checkout the main branch (production), because we want to develop from a stable release:
```
git checkout main
```

Now we need to create a new branch, for this example I am developing a bugfix (use the type as a prefix for you branch name):
```
git checkout -b bugfix-logging-problems
```
(maybe in the future we want to use ticket codes in the branch name for reference)

You are now mostly done for the branching part and can start developing your beautiful fixes or features.

When you are done writing your magnificent code, commit and push your changes to remote:
```
git commit -am "Fixed aws configuration for logger class" && git push origin bugfix-logging-problems
```

Great job, well.. almost! Because how do your teammates know you have written new code for us to check out?
You go to gitlab.com and create a new merge request in the repository you were working in.

Select the source branch (which is the branch you named and created) and automatically will the `staging` branch be selected as the source (which is correct).
You'll never push or merge code to the `main` branch, always to the `staging` branch.
For both branches we have pipelines in place, which will deploy to the corresponding website and we'd like to test your code first before production is automatically updated.

Enjoy coding!